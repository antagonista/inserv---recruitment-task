import Vue from 'vue';
import App from '../components/App';
import axios from 'axios';
import VueAxios from 'vue-axios';

Vue.use(VueAxios, axios);

Vue.config.productionTip = false;

const app = new Vue({
    el: '#app',
    render: h => h(App)
});