<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

class MainController extends AbstractController
{
    /**
     * Render main view (Vue Form)
     *
     * @Route("/", name="mainForm")
     */
    public function index()
    {
        return $this->render('index.html.twig');
    }

    /**
     * Transform Request (Json String) to array and return
     *
     * @Route("/submitForm",
     *     name="submitForm",
     *     methods={"POST"}
     * )
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function submitForm(Request $request) : JsonResponse
    {

        $json = $request->getContent();
        $data = json_decode($json, true);
        $data = preg_split('/\r\n|[\r\n]/', $data['textarea']);

        return new JsonResponse($data,200);
    }
}
